open Ocamlbuild_plugin

let () =
  dispatch begin function
  | Before_options ->
    Options.use_ocamlfind := true
  | _ -> ()
  end
